from Bio import Entrez, SeqIO
import os.path
# Configurar les API keys per accedir als serveis de NCBI
Entrez.email = "olgo3000@office.proven.cat"

def buscar_accession_id(gen, organismes):
    accession_ids = {}

    for organisme in organismes:
        # Realitzar una cerca per l'organisme i el gen específic
        query = f'{organisme}[Organism] AND {gen}[Gene]'
        handle = Entrez.esearch(db='nucleotide', term=query, retmax=1)
        result = Entrez.read(handle)

        # Afegir l'Accession ID al diccionari
        if 'IdList' in result and result['IdList']:
            accession_ids[organisme] = result['IdList'][0]

    return accession_ids

def descarregar_genbank_si_cal(accession_ids):
    for organisme, accession_id in accession_ids.items():
        # Verificar si el fitxer ja existeix localment
        filename = f'{organisme.replace(" ", "_")}_{accession_id}.gb'
        if os.path.isfile(filename):
            print(f"El fitxer GenBank per {organisme} ja existeix: {filename}")
        else:
            # Descarregar el fitxer GenBank utilitzant l'Accession ID
            handle = Entrez.efetch(db='nucleotide', id=accession_id, rettype='gb', retmode='text')
            genbank_data = handle.read()

            # Guardar el fitxer GenBank amb un nom específic
            with open(filename, 'w') as file:
                file.write(genbank_data)
            print(f"Fitxer GenBank per {organisme}: {filename}")


            # Imprimir la taxonomia
            with open(filename, 'r') as file:
                record = SeqIO.read(file, 'genbank')
                taxonomia = record.annotations['taxonomy']
                print(f"Taxonomia per {organisme}: {', '.join(taxonomia)}")



# Organismes d'interès i gen TP53
organismes_interes = [
    "Homo sapiens",
    "Bos taurus",
    "Pan troglodytes",
    "Caenorhabditis elegans"
]

gen_comu = "TP53"

# Buscar Accession IDs
accession_ids = buscar_accession_id(gen_comu, organismes_interes)

# Descarregar fitxers GenBank
descarregar_genbank_si_cal(accession_ids)