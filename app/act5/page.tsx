'use client';
import seq from "bionode-seq";
import React, { useState } from "react";


const TranslationPage = () => {
  const [dnaInput, setDnaInput] = useState("");
  const [rnaOutput, setRnaOutput] = useState("");
  const [proteinOutput, setProteinOutput] = useState("");
  //const [cantitatCG, setcantitatCG] = useState("");



  const handleInputChange = (e) => {
    const { value } = e.target;
    value.toUpperCase();

    // Comprobar bases válidas
    const isValidInput = /^[ACGT]+$/.test(value);

    if (isValidInput) {
      
    setDnaInput(value);
}


    const rnaResult = transcribeDNA(value);
    setRnaOutput(rnaResult);


 
    const proteinResult = translateRNA(rnaResult);
    setProteinOutput(proteinResult);
  };




  const transcribeDNA = (dnaSequence) => {

    const seq = require("bionode-seq");
    return seq.transcribe(dnaSequence);
  };


  const translateRNA = (rnaSequence) => {

    const seq = require("bionode-seq");
    return seq.translate(rnaSequence);
  };

  const calculateGCPercentage = (seq) => {
    const gcCount = (seq.match(/[GC]/g) || []).length;
    const totalBases = seq.length;
    return totalBases > 0 ? (gcCount / totalBases) * 100 : 0;
  };

  const gcPercentage = calculateGCPercentage(dnaInput);

  

  return (
    <div className="p-6">
      <h1 className="text-indigo-700 text-xl font-bold">Translation Page</h1>
      <div className="mt-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">Enter DNA Sequence:</label>
        <input
          type="text"
          value={dnaInput}
          onChange={handleInputChange}
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        />
      </div>
      <div className="mt-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">Resulting RNA Sequence:</label>
        <input
          type="text"
          value={rnaOutput}
          readOnly
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        />
      </div>
      <div className="mt-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">Resulting Protein Sequence:</label>
        <input
          type="text"
          value={proteinOutput}
          readOnly
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        />
      </div>
      <div className="mt-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">El percentatge GC :</label>
        <input
          type="text"
          value={gcPercentage.toFixed(2)}
          readOnly
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"

        />
      </div>
    </div>
  );
};


export default TranslationPage;
